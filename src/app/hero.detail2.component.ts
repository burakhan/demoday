/**
 * Created by Burakhan on 05/08/2017.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Hero, HEROES } from './hero';

@Component({
  template: `
      <div *ngIf="hero">
          <h2>id : {{ hero.id }} / name : {{ hero.name }}</h2>

      </div>
  `
})
export class HeroDetail2Component implements OnDestroy {
  id: number;
  hero: Hero;

  constructor(private route: ActivatedRoute) {

    route.params.subscribe((param) => {
      this.calis();
    });

    route.params.forEach((param) => {
      this.id = +param['id']; // Number()
    });
  }

  calis() {
    let result = HEROES.filter((item) => {
      return item.id === this.id
    });

    this.hero = result[0];
  }

  ngOnDestroy(): void {
  }
}