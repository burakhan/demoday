import { Component } from '@angular/core';
import { Hero, HEROES } from './hero';
/**
 * Created by Burakhan on 05/08/2017.
 */

@Component({
  templateUrl: './new.hero.component.html',
})
export class NewHeroComponent {
    newHero:Hero;

    constructor() {
      this.newHero = new Hero;
    }
    add(){
      let count = HEROES.length + 50;
      this.newHero.id = count;
      HEROES.push(this.newHero);
      this.newHero = new Hero;
    }
}
