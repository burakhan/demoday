import { Component } from '@angular/core';
import { Hero, HEROES } from './hero';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  companyName = 'idvlabs';
  heroList = HEROES;
  selectedHero: Hero;

  hero: Hero = {
    id: 5,
    name: 'Mehmet Surav'
  };

  tikladim(item: Hero) {
    this.selectedHero = item;

  }
}
