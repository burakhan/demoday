import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { AppComponent } from './app.component';
import { HeroDetailComponent } from './hero.detail.component';
import { RouterModule } from '@angular/router';
import { NewHeroComponent } from './new.hero.component';
import { HeroDetail2Component } from './hero.detail2.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroDetailComponent,
    NewHeroComponent,
    HeroDetail2Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      {
        path: 'new-hero',
        component: NewHeroComponent
      },
      {
        path: 'detail/:id',
        component: HeroDetail2Component
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
