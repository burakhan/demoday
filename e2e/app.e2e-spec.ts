import { DemodayPage } from './app.po';

describe('demoday App', () => {
  let page: DemodayPage;

  beforeEach(() => {
    page = new DemodayPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
